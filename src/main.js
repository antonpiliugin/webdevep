import Vue from 'vue'
import App from './App.vue'
import UniqueId from '../plugins/uniqueId'
import Toastify from '../plugins/toastify'
import VueAxios from '../plugins/vueAxios'
import DateTimeFormat from '../plugins/dateTimeFormat'

Vue.config.productionTip = false

Vue.use(UniqueId)
Vue.use(Toastify)
Vue.use(...VueAxios)
Vue.use(DateTimeFormat)

new Vue({
  render: h => h(App),
}).$mount('#app')
