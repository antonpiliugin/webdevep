import axios from 'axios'
import VueAxios from 'vue-axios'

export default [VueAxios, axios]