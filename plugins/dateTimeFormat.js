import { parseISO } from 'date-fns'
import { format, utcToZonedTime } from 'date-fns-tz'

export default {
  install (Vue) {
    Vue.prototype.dateTimeFormat = (dateTime, outputFormat='yyyy-MM-dd HH:mm:ss', inputFormat='UTC') => {
      const parsedTime = parseISO(dateTime)
      const formatInTimeZone = (date, fmt, tz) => format(utcToZonedTime(date, tz), fmt, { timeZone: tz })
      return formatInTimeZone(parsedTime, outputFormat, inputFormat)
    }
  }
}