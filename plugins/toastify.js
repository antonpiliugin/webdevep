import Toastify from 'toastify-js'
import 'toastify-js/src/toastify.css'

export default {
  install (Vue) {
    Vue.prototype.toast = (message, type) => {
      const styles = {
        info: {
          background: '#7EA1C9'
        },
        error: {
          background: '#B82200'
        },
        success: {
          background: '#38943E'
        },
        default: {
          background: '#23395B'
        }
      }
  
      Toastify({
        text: message,
        duration: 5000,
        close: false,
        gravity: 'bottom',
        position: 'right',
        stopOnFocus: true,
        style: styles[type],
        onClick: function() {} // Callback after click
      }).showToast()
    }
  }
}