module.exports = {
  devServer: {
    proxy: {
      '/login/': {
        target: 'https://charmium.ru/auth-back/api/v2/',
        logLevel: 'debug'
      },
      '/putInboxMessage/': {
        target: 'https://charmium.ru/noty/api/private/',
        logLevel: 'debug'
      },
      '/getInboxMessages/': {
        target: 'https://charmium.ru/noty/api/v1/',
        logLevel: 'debug'
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/assets/scss/_variables.scss";
          @import "@/assets/scss/_mixins.scss";
        `
      }
    }
  }
}